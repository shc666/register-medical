<?php

namespace App\Http\Controllers;

class LaporanController extends Controller 
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:laporan-manage');
    }

    /**
     * Index Laporan Rekapitulasi.
     *
     * @return void
     */
    public function index()
    {
        return view('laporan.index');
    }
}