<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Hash;
use Auth;
use Image;

class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Index Profile.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = User::find(Auth::user()->id);
        return view('profile.detail', [
            'user' => $user,
            'edit' => true
        ]);
    }

    /**
     * Update profile details.
     *
     * @param ProfileUpdateRequest $request
     * @return mixed
     */
    public function profileUpdate(Request $request)
    {
        $input = $request->all();
        $data = \Input::except(array('_token'));
        $rule = array(
            'name'              => 'required',
            'profile_avatar.*'  => 'sometimes|mimes:jpeg,jpg,png|max:3000',
        );
        $messages = [
            'name.required'         => 'Bagian nama lengkap dibutuhkan',
            'profile_avatar.mimes'  => 'Upload avatar harus berupa format (.jpg, .jpeg, .png)',
            'profile_avatar.max'    => 'Upload avatar hanya diperbolehkan dengan ukuran maksimal 2 Mb',
        ];
        $validator = \Validator::make($data,$rule,$messages);
        if($validator->fails())
        {
            return response()->json([
                'errors' => $validator->errors()->all()
            ]);
        }

        $user = User::find(Auth::user()->id);
        $user->update($input);

        if($request->hasFile('profile_avatar')){
    		$avatar = $request->file('profile_avatar');
    		$filename = time() . '.' . $avatar->getClientOriginalExtension();
    		Image::make($avatar)->resize(300, 300)->save('upload/avatars/' . $filename);

    		$user = Auth::user();
    		$user->avatar = $filename;
            $user->save();
            
            return response()->json([
                'success' => "Berhasil mengupload gambar"
            ]);
    	}

        return response()->json([
            'success' => true,
            'message' => "Berhasil mengubah data"
        ]);
    }

    /**
     * Update account details.
     *
     * @param AccountUpdateRequest $request
     * @return mixed
     */
    public function accountUpdate(Request $request)
    {
        $input = $request->all();
        $data = \Input::except(array('_token'));
        $rule = array(
            'username'  => 'required',
            'email'     => 'required|email|unique:users,email'
        );
        $messages = [
            'username.required' => 'Bagian username dibutuhkan',
            'email.required'    => 'Bagian email dibutuhkan',
            'email.unique'      => 'Email sudah terisi dengan nilai yang sama. Silahkan masukan input yang berbeda',
        ];
        $validator = \Validator::make($data,$rule,$messages);
        if($validator->fails())
        {
            return response()->json([
                'errors' => $validator->errors()->all()
            ]);
        }
        
        $user = User::find(Auth::user()->id);
        $user->update($input);

        return response()->json([
            'success' => true,
            'message' => "Berhasil mengubah data"
        ]);
    }

    /**
     * Update password details.
     *
     * @param PasswordUpdateRequest $request
     * @return mixed
     */
    public function passwordUpdate(Request $request)
    {
        $input = $request->all();
        $data = \Input::except(array('_token'));
        $rule = array(
            'password'  => 'same:confirm'
        );
        $messages = [
            'password.same'  => 'Bagian konfirmasi password harus sama dengan password'
        ];
        $validator = \Validator::make($data,$rule,$messages);
        if($validator->fails())
        {
            return response()->json([
                'errors' => $validator->errors()->all()
            ]);
        }

        $user = User::find(Auth::user()->id);
        if(!empty($input['password']))
        {
            $input['password'] = Hash::make($input['password']);
        }
        else
        {
            $input = array_except($input,array('password'));
        }

        $user->update($input);

        return response()->json([
            'success' => true,
            'message' => "Berhasil mengubah password"
        ]);
    }
}