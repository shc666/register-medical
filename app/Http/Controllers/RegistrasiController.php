<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use App\Http\Requests\Registrasi\Balita\CreateRequestBalita;
use App\Http\Requests\Registrasi\IbuHamil\CreateRequestIbuHamil;
use App\Http\Requests\Registrasi\WusPus\CreateRequestWusPus;
use App\Helpers\IndonesianTime;
use App\TBayiBalita;
use App\TIbuHamil;
use App\TWusPus;
use App\MDasawisma;
use App\TBayiPoliData;
use App\TIbuPoliData;
use App\TWusPusPoliData;
use Carbon\Carbon;
use DB;

class RegistrasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:registration-manage');
    }

    /**
     * Function For Generate Poli Data
     */
    private function genPoliData($birthday, $n)
    {
        $birthdays = [];
        for($i=1; $i<=$n; $i++)
        {
            $date = strtotime($birthday);
            $date = date("Y-m-d", strtotime("+$i month", $date));
            $birthdays[] = $date;
        }

        return $birthdays;
    }

    /*
     * Function Get Type Registration 
     */
    public function getTipeRegistrasi($int)
    {
        $types = [
            1 => "Bayi dan Balita",
            2 => "Ibu Hamil",
            3 => "WUS dan PUS"
        ];
        foreach($types as $key => $value)
        {
            if($key == $int)
            {
                return $value;
            }
        }
        
        return abort(404);
    }

    /**
     * Index Registration a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexRegistrasi($tipe)
    {
        $header = $this->getTipeRegistrasi($tipe);

        return view('registrasi.index-registrasi', compact('header', 'tipe'));
    }

    /**
     * Index Poli a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexPoli($tipe, $id)
    {
        $header = $this->getTipeRegistrasi($tipe);

        return view('poli.index-poli', compact('header', 'tipe'));
    }

    /**
     * Create a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($tipe)
    {
        $header = $this->getTipeRegistrasi($tipe);

        return view('registrasi.add', compact('header'));
    }

    /**
     * Edit the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($tipe, $id)
    {
        $header = $this->getTipeRegistrasi($tipe);

        return view('poli.edit', compact('header'));
    }

    /**
     * Display a listing of the resource for Vue.
     *
     * @return \Illuminate\Http\Response
     */
    public function getData($tipe)
    {
        $header = $this->getTipeRegistrasi($tipe);
        $dasawisma = MDasawisma::all();

        return response()->json([
            'header'    => $header,
            'tipe'      => $tipe,
            'dasawisma' => $dasawisma
        ]);
    }

    /**
     * Display a registration listing of the resource for Vue.
     *
     * @return \Illuminate\Http\Response
     */
    public function getDataRegistrasi(Request $request)
    {
        $search_keywords  = Input::get('search_keywords', null);
        $tipe = $request->tipe;
        $header = $this->getTipeRegistrasi($tipe);
        DB::statement(DB::raw('set @rownum = 0'));
        if($tipe == 1)
        {
            if($search_keywords != '')
            {
                $query = TBayiBalita::where(function ($query) use ($search_keywords) {
                        $query->where('nama_bayi', "LIKE", "%$search_keywords%")
                            ->orWhere("nama_ibu", "LIKE", "%$search_keywords%")
                            ->orWhere("nama_ayah", "LIKE", "%$search_keywords%");
                        })
                        ->paginate(10);
                
                if($request) {
                    $query->appends($request->all());
                }

                return response()->json([
                    'status'          => true,
                    'header'          => $header,
                    'tipe'            => $tipe,
                    'data_registrasi' => $query
                ]);
            }
            else
            {
                $registration = TBayiBalita::select(
                                    DB::raw('@rownum := @rownum + 1 as no'),
                                    't_bayi_balita.*'
                                )
                                ->orderBy('id', 'desc')
                                ->paginate(10);

                return response()->json([
                    'header'          => $header,
                    'tipe'            => $tipe,
                    'data_registrasi' => $registration
                ]);
            }
        }
        elseif($tipe == 2)
        {
            if($search_keywords != '')
            {
                $query = TIbuHamil::where(function ($query) use ($search_keywords) {
                        $query->where('nama', "LIKE", "%$search_keywords%")
                            ->orWhere("nama_suami", "LIKE", "%$search_keywords%");
                        })
                        ->paginate(10);
                
                if($request) {
                    $query->appends($request->all());
                }

                return response()->json([
                    'status'          => true,
                    'header'          => $header,
                    'tipe'            => $tipe,
                    'data_registrasi' => $query
                ]);
            }
            else
            {
                $registration = TIbuHamil::select(
                                    DB::raw('@rownum := @rownum + 1 as no'),
                                    't_ibu_hamil.*'
                                )
                                ->orderBy('id', 'desc')
                                ->paginate(10);

                return response()->json([
                    'header'          => $header,
                    'tipe'            => $tipe,
                    'data_registrasi' => $registration
                ]);
            }
        }
        else
        {
            if($search_keywords != '')
            {
                $query = TWusPus::where(function ($query) use ($search_keywords) {
                        $query->where('nama', "LIKE", "%$search_keywords%")
                            ->orWhere("nama_suami", "LIKE", "%$search_keywords%");
                        })
                        ->paginate(10);
                
                if($request) {
                    $query->appends($request->all());
                }

                return response()->json([
                    'status'          => true,
                    'header'          => $header,
                    'tipe'            => $tipe,
                    'data_registrasi' => $query
                ]);
            }
            else
            {
                $registration = TWusPus::select(
                                    DB::raw('@rownum := @rownum + 1 as no'),
                                    't_wus_pus.*'
                                )
                                ->orderBy('id', 'desc')
                                ->paginate(10);

                return response()->json([
                    'header'          => $header,
                    'tipe'            => $tipe,
                    'data_registrasi' => $registration
                ]);
            }
        }
    }

    /**
     * Display a poli listing of the resource for Vue.
     *
     * @return \Illuminate\Http\Response
     */
    public function getDataPoli(Request $request)
    {
        $tipe = $request->tipe;
        $data_id = $request->reg_id;
        $header = $this->getTipeRegistrasi($tipe);
        DB::statement(DB::raw('set @rownum = 0'));
        if($tipe == 1)
        {
            $bayi = TBayiBalita::select('nama_bayi')->where('id', $data_id)->first();
            $poli = TBayiBalita::join('t_bayi_balita_polidata as tbp', 'tbp.bayi_id', '=', 't_bayi_balita.id')
                            ->select(
                                DB::raw('@rownum := @rownum + 1 as no'),
                                't_bayi_balita.id as id_registrasi',
                                'tbp.*',
                                DB::raw('MONTHNAME(tbp.jadwal) as month_name')
                            )
                            ->where('t_bayi_balita.id', $data_id)
                            ->orderBy('t_bayi_balita.id', 'asc')
                            ->paginate(20);

            foreach($poli as $key => $value)
            {
                $value->month_name = IndonesianTime::toIndonesiaMonth($value->jadwal);
            }

            return response()->json([
                'header'    => $header,
                'tipe'      => $tipe,
                'nama_bayi' => $bayi,
                'data_poli' => $poli
            ]);
        }
        elseif($tipe == 2)
        {
            $ibu = TIbuHamil::select('nama')->where('id', $data_id)->first();
            $poli = TIbuHamil::join('t_ibu_hamil_polidata as tip', 'tip.ibu_id', '=', 't_ibu_hamil.id')
                            ->select(
                                DB::raw('@rownum := @rownum + 1 as no'),
                                't_ibu_hamil.id as id_registrasi',
                                'tip.*',
                                DB::raw('MONTHNAME(tip.jadwal) as month_name')
                            )
                            ->where('t_ibu_hamil.id', $data_id)
                            ->orderBy('t_ibu_hamil.id', 'asc')
                            ->paginate(20);

            foreach($poli as $key => $value)
            {
                $value->month_name = IndonesianTime::toIndonesiaMonth($value->jadwal);
            }

            return response()->json([
                'header'    => $header,
                'tipe'      => $tipe,
                'nama_ibu'  => $ibu,
                'data_poli' => $poli
            ]);
        }
        else
        {
            $name = TWusPus::select('nama')->where('id', $data_id)->first();
            $poli = TWusPus::join('t_wus_pus_polidata as twp', 'twp.people_id', '=', 't_wus_pus.id')
                    ->select(
                        DB::raw('@rownum := @rownum + 1 as no'),
                        't_wus_pus.id as id_registrasi',
                        'twp.*'
                    )
                    ->where('t_wus_pus.id', $data_id)
                    ->orderBy('t_wus_pus.id', 'asc')
                    ->paginate(20);

            return response()->json([
                'header'    => $header,
                'tipe'      => $tipe,
                'name'      => $name,
                'data_poli' => $poli
            ]);
        }
    }

    public function getPrevData(Request $request)
    {
        $tipe = $request->tipe;
        $data_id = $request->id;
        $header = $this->getTipeRegistrasi($tipe);
        if($tipe == 1)
        {
            $previous = TBayiPoliData::select('berat', 'panjang')
                        ->where('id', '<', $data_id)
                        ->orderBy('id', 'desc')
                        ->first();
            
            $data = TBayiPoliData::select(
                        DB::raw('MONTHNAME(jadwal) as month_name'),
                        't_bayi_balita_polidata.*'
                    )
                    ->where('id', $data_id)
                    ->orderBy('id', 'desc')
                    ->first();
            
            $data['month_name'] = IndonesianTime::toIndonesiaMonth($data['jadwal']);

            return response()->json([
                'header'    => $header,
                'tipe'      => $tipe,
                'prev_data' => $previous,
                'data'      => $data
            ]);
        }
        elseif($tipe == 2)
        {
            $previous = TIbuPoliData::select('berat', 'lila')
                        ->where('id', '<', $data_id)
                        ->orderBy('id', 'desc')
                        ->first();

            $data = TIbuPoliData::select(
                        DB::raw('MONTHNAME(jadwal) as month_name'),
                        't_ibu_hamil_polidata.*'
                    )
                    ->where('id', $data_id)
                    ->orderBy('id', 'desc')
                    ->first();
            
            $data['month_name'] = IndonesianTime::toIndonesiaMonth($data['jadwal']);

            return response()->json([
                'header'    => $header,
                'tipe'      => $tipe,
                'prev_data' => $previous,
                'data'      => $data
            ]);
        }
        else
        {
            $data = TWusPusPoliData::where('id', $data_id)
                    ->orderBy('id', 'desc')
                    ->first();

            return response()->json([
                'header'    => $header,
                'tipe'      => $tipe,
                'data'      => $data
            ]);
        }
    }

    /**
     * Store instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeBalita(CreateRequestBalita $request)
    {
        $inputs = $request->all();
        $listings = TBayiBalita::create($inputs);

        if(!empty($listings))
        {
            $poli = $this->genPoliData($listings->tgl_lahir_bayi, 60);
            foreach($poli as $key => $value)
            {
                $data_poli = new TBayiPoliData;
                if($key == 0)
                {
                    $data_poli->bayi_id = $listings->id;
                    $data_poli->is_set  = 1;
                    $data_poli->berat   = $listings->berat_lahir_bayi;
                    $data_poli->panjang = $listings->panjang_lahir_bayi;
                    $data_poli->jadwal  = $value;
                    $data_poli->save();
                }
                $data_poli->bayi_id = $listings->id;
                $data_poli->jadwal  = $value;
                $data_poli->save();         
            }

            return response()->json([
                'status'    => true,
                'listings'  => $listings->id,
                'message'   => 'Berhasil menyimpan data'
            ], 200);
        }
        else
        {
            return response()->json([
                'status'    => false,
                'errors'    => [
                    'pesan' => 'Gagal menyimpan! Telah terjadi kesalahan...'
                ]
            ], 500);
        }
    }

    /**
     * Store instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeIbuHamil(CreateRequestIbuHamil $request)
    {
        $inputs = $request->all();
        $age = Carbon::parse($inputs['tgl_lahir'])->age;

        $listings = new TIbuHamil;
        $listings->dasawisma_id   = $inputs['dasawisma_id'];
        $listings->tgl_entry      = $inputs['tgl_entry'];
        $listings->nik            = $inputs['nik'];
        $listings->nama           = $inputs['nama'];
        $listings->nama_suami     = $inputs['nama_suami'];
        $listings->tgl_lahir      = $inputs['tgl_lahir'];
        $listings->tgl_hamil      = $inputs['tgl_hamil'];
        $listings->hamil_count    = $inputs['hamil_count'];
        $listings->is_risk        = $inputs['is_risk'];
        $listings->umur           = $age;
        $listings->umur_kehamilan = $inputs['umur_kehamilan'];
        $listings->save();

        if(!empty($listings))
        {
            $poli = $this->genPoliData($listings->tgl_hamil, 9);
            foreach($poli as $key => $value)
            {
                $data_poli = new TIbuPoliData;
                $data_poli->ibu_id  = $listings->id;
                $data_poli->jadwal  = $value;
                $data_poli->save();         
            }

            return response()->json([
                'status'    => true,
                'listings'  => $listings->id,
                'message'   => 'Berhasil menyimpan data',
            ], 200);
        }
        else
        {
            return response()->json([
                'status'    => false,
                'errors'    => [
                    'pesan' => 'Gagal menyimpan! Telah terjadi kesalahan...'
                ]
            ], 422);
        }
    }

    /**
     * Store instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeWusPus(CreateRequestWusPus $request)
    {
        $inputs = $request->all();
        $age = Carbon::parse($inputs['tgl_lahir'])->age;

        $listings = new TWusPus;
        $listings->dasawisma_id   = $inputs['dasawisma_id'];
        $listings->tgl_entry      = $inputs['tgl_entry'];
        $listings->nik            = $inputs['nik'];
        $listings->nama           = $inputs['nama'];
        $listings->nama_suami     = $inputs['nama_suami'];
        $listings->tgl_lahir      = $inputs['tgl_lahir'];
        $listings->umur           = $age;
        $listings->tahapan_ks     = $inputs['tahapan_ks'];
        $listings->save();

        if(!empty($listings))
        {
            $data_poli = new TWusPusPoliData;
            $data_poli->people_id  = $listings->id;
            $data_poli->save();    

            return response()->json([
                'status'    => true,
                'listings'  => $listings->id,
                'message'   => 'Berhasil menyimpan data',
            ], 200);
        }
        else
        {
            return response()->json([
                'status'    => false,
                'errors'    => [
                    'pesan' => 'Gagal menyimpan! Telah terjadi kesalahan...'
                ]
            ], 422);
        }
    }

    /**
     * Store instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storePoliBalita(Request $request)
    {
        $inputs = $request->all();
        $listings = TBayiPoliData::find($inputs['id']);
        $listings->berat       = $inputs['berat'];
        $listings->panjang     = $inputs['panjang'];
        $listings->asi         = $inputs['asi'];
        $listings->vitamin_a1  = $inputs['vitamin_a1'];
        $listings->vitamin_a2  = $inputs['vitamin_a2'];
        $listings->oralit      = $inputs['oralit'];
        $listings->imun_hbo    = $inputs['imun_hbo'];
        $listings->imun_bcg    = $inputs['imun_bcg'];
        $listings->imun_dpthb1 = $inputs['imun_dpthb1'];
        $listings->imun_dpthb2 = $inputs['imun_dpthb2'];
        $listings->imun_dpthb3 = $inputs['imun_dpthb3'];
        $listings->imun_polio1 = $inputs['imun_polio1'];
        $listings->imun_polio2 = $inputs['imun_polio2'];
        $listings->imun_polio3 = $inputs['imun_polio3'];
        $listings->imun_polio4 = $inputs['imun_polio4'];
        $listings->imun_campak = $inputs['imun_campak'];
        $listings->is_set      = 1;
        $listings->save();

        if(!empty($listings))
        {
            return response()->json([
                'status'    => true,
                'message'   => 'Berhasil menyimpan data!'
            ], 200);
        }
        else
        {
            return response()->json([
                'status'    => false,
                'errors'    => [
                    'pesan' => 'Gagal menyimpan! Telah terjadi kesalahan...'
                ]
            ], 422);
        }
    }

    /**
     * Store instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storePoliIbuHamil(Request $request)
    {
        $inputs = $request->all();
        $listings = TIbuPoliData::find($inputs['id']);
        $listings->berat          = $inputs['berat'];
        $listings->lila           = $inputs['lila'];
        $listings->pmt            = $inputs['pmt'];
        $listings->tambah_darah_1 = $inputs['tambah_darah_1'];
        $listings->tambah_darah_2 = $inputs['tambah_darah_2'];
        $listings->tambah_darah_3 = $inputs['tambah_darah_3'];
        $listings->imunisasi_tt_1 = $inputs['imunisasi_tt_1'];
        $listings->imunisasi_tt_2 = $inputs['imunisasi_tt_2'];
        $listings->imunisasi_tt_3 = $inputs['imunisasi_tt_3'];
        $listings->imunisasi_tt_4 = $inputs['imunisasi_tt_4'];
        $listings->imunisasi_tt_5 = $inputs['imunisasi_tt_5'];
        $listings->vitamin_a      = $inputs['vitamin_a'];
        $listings->catatan        = $inputs['catatan'];
        $listings->is_set         = 1;
        $listings->save();

        if(!empty($listings))
        {
            return response()->json([
                'status'    => true,
                'message'   => 'Berhasil menyimpan data!'
            ], 200);
        }
        else
        {
            return response()->json([
                'status'    => false,
                'errors'    => [
                    'pesan' => 'Gagal menyimpan! Telah terjadi kesalahan...'
                ]
            ], 422);
        }
    }

    /**
     * Store instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storePoliWusPus(Request $request)
    {
        $inputs = $request->all();
        $listings = TWusPusPoliData::find($inputs['id']);
        $listings->anak_hidup                    = $inputs['anak_hidup'];
        $listings->anak_meninggal                = $inputs['anak_meninggal'];
        $listings->lila                          = $inputs['lila'];
        $listings->imunisasi_tt_1                = $inputs['imunisasi_tt_1'];
        $listings->imunisasi_tt_2                = $inputs['imunisasi_tt_2'];
        $listings->imunisasi_tt_3                = $inputs['imunisasi_tt_3'];
        $listings->imunisasi_tt_4                = $inputs['imunisasi_tt_4'];
        $listings->imunisasi_tt_5                = $inputs['imunisasi_tt_5'];
        $listings->jenis_kontrasepsi_pakai       = $inputs['jenis_kontrasepsi_pakai'];
        $listings->tanggal_penggantian           = $inputs['tanggal_penggantian'];
        $listings->jenis_kontrasepsi_penggantian = $inputs['jenis_kontrasepsi_penggantian'];
        $listings->is_set                        = 1;
        $listings->save();

        if(!empty($listings))
        {
            return response()->json([
                'status'    => true,
                'message'   => 'Berhasil menyimpan data!'
            ], 200);
        }
        else
        {
            return response()->json([
                'status'    => false,
                'errors'    => [
                    'pesan' => 'Gagal menyimpan! Telah terjadi kesalahan...'
                ]
            ], 422);
        }
    }

    /**
     * Destroy instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroyRegistrasiBalita($id)
    {
        $listing = TBayiBalita::find($id);
        $listing->delete();

        return response()->json([
            'status'    => true,
            'message'   => 'Berhasil menghapus data!'
        ]);
    }

    /**
     * Destroy instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroyRegistrasiIbuHamil($id)
    {
        $listing = TIbuHamil::find($id);
        $listing->delete();

        return response()->json([
            'status'    => true,
            'message'   => 'Berhasil menghapus data!'
        ]);
    }

    /**
     * Destroy instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroyRegistrasiWusPus($id)
    {
        $listing = TWusPus::find($id);
        $listing->delete();

        return response()->json([
            'status'    => true,
            'message'   => 'Berhasil menghapus data!'
        ]);
    }
}