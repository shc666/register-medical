<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\MasterData\Banjar\CreateRequest;
use App\Http\Requests\MasterData\Banjar\UpdateRequest;
use App\MBanjar;
use DataTables;

class BanjarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:master-data-manage');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('master-data.banjar.index');
    }

    public function getData()
    {
        $query_data = MBanjar::all();

        return DataTables::of($query_data)
                ->addIndexColumn()
                ->addColumn('action', function ($query_data) {
                    return '
                        <a class="btn btn-warning btn-sm" data-skin="dark" data-toggle="kt-tooltip" data-placement="top" title="Ubah" data-original-title="Dark skin" href="'.route('banjar.edit', $query_data->id).'">
                            <i class="flaticon2-edit"></i>
                        </a>
                        <button type="button" class="btn btn-danger btn-sm" data-skin="dark" data-toggle="kt-tooltip" data-placement="top" title="Hapus" data-original-title="Dark skin" onclick="Delete.data('.$query_data->id.')">
                            <i class="flaticon-delete"></i>
                        </button>
                    ';
                })
                ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $edit = false;

        return view('master-data.banjar.add-edit', compact('edit'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
        $input = $request->all();
        MBanjar::create($input);

        return redirect()->route('banjar.index')
        ->withSuccess('Berhasil Membuat Data Baru!');
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit = true;
        $data = MBanjar::find($id);

        return view('master-data.banjar.add-edit', compact('edit', 'data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        $input = $request->all();
        $data = MBanjar::find($id);
        $data->update($input);

        return redirect()->route('banjar.index')
        ->withSuccess('Berhasil Mengubah Data!');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = MBanjar::find($id)->delete();
        
        return response()->json($data);
    }
}