<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\MasterData\Dasawisma\CreateRequest;
use App\Http\Requests\MasterData\Dasawisma\UpdateRequest;
use App\MDasawisma;
use App\MBanjar;
use DataTables;

class DasawismaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:master-data-manage');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('master-data.dasawisma.index');
    }

    public function getData()
    {
        $query_data = MDasawisma::join('m_banjar as mb', 'mb.id', '=', 'm_dasawisma.banjar_id')
                        ->select(
                            'm_dasawisma.*',
                            'mb.nama_banjar'
                        )
                        ->get();

        return DataTables::of($query_data)
                ->addIndexColumn()
                ->addColumn('action', function ($query_data) {
                    return '
                        <a class="btn btn-warning btn-sm" data-skin="dark" data-toggle="kt-tooltip" data-placement="top" title="Ubah" data-original-title="Dark skin" href="'.route('dasawisma.edit', $query_data->id).'">
                            <i class="flaticon2-edit"></i>
                        </a>
                        <button type="button" class="btn btn-danger btn-sm" data-skin="dark" data-toggle="kt-tooltip" data-placement="top" title="Hapus" data-original-title="Dark skin" onclick="Delete.data('.$query_data->id.')">
                            <i class="flaticon-delete"></i>
                        </button>
                    ';
                })
                ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $edit = false;
        $banjars = MBanjar::all();

        return view('master-data.dasawisma.add-edit', compact('edit', 'banjars'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
        $input = $request->all();
        MDasawisma::create($input);

        return redirect()->route('dasawisma.index')
        ->withSuccess('Berhasil Membuat Data Baru!');
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit = true;
        $banjars = MBanjar::all();
        $data = MDasawisma::find($id);

        return view('master-data.dasawisma.add-edit', compact('edit', 'data', 'banjars'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        $input = $request->all();
        $data = MDasawisma::find($id);
        $data->update($input);

        return redirect()->route('dasawisma.index')
        ->withSuccess('Berhasil Mengubah Data!');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = MDasawisma::find($id)->delete();
        
        return response()->json($data);
    }
}