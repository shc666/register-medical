<?php

namespace App\Http\Requests\Registrasi\WusPus;

use App\Http\Requests\Request;

class CreateRequestWusPus extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'dasawisma_id'  => 'required',
            'tgl_entry'     => 'required',
            'nik'           => 'required|digits:16',
            'nama'          => 'required',
            'tgl_lahir'     => 'required'
        ];
    }

    public function messages()
    {
        return [
            'dasawisma_id.required'  => 'Bagian daerah dasawisma tidak boleh kosong',
            'tgl_entry.required'     => 'Bagian tanggal entry tidak boleh kosong',
            'nik.required'           => 'Bagian NIK tidak boleh kosong',
            'nik.digits'             => 'Bagian NIK hanya boleh diisi dengan angka dan berjumlah harus 16',
            'nama.required'          => 'Bagian nama tidak boleh kosong',
            'nama_suami.required'    => 'Bagian nama suami tidak boleh kosong',
            'tgl_lahir.required'     => 'Bagian tanggal lahir tidak boleh kosong'
        ];
    }
}