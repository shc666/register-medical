<?php

namespace App\Http\Requests\Registrasi\Balita;

use App\Http\Requests\Request;

class CreateRequestBalita extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'dasawisma_id'       => 'required',
            'tgl_entry'          => 'required',
            'nik'                => 'required|unique:t_bayi_balita,nik|digits:16',
            'nama_bayi'          => 'required|unique:t_bayi_balita,nama_bayi',
            'nama_ibu'           => 'required',
            'nama_ayah'          => 'required',
            'tgl_lahir_bayi'     => 'required',
            'jenis_kelamin_bayi' => 'required',
            'berat_lahir_bayi'   => 'required',
            'panjang_lahir_bayi' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'dasawisma_id.required'        => 'Bagian daerah dasawisma tidak boleh kosong',
            'tgl_entry.required'           => 'Bagian tanggal entry tidak boleh kosong',
            'nik.required'                 => 'Bagian NIK tidak boleh kosong',
            'nik.unique'                   => 'Bagian NIK sudah terisi dengan nilai yang sama di dalam database, silahkan isi dengan nilai yang berbeda',
            'nik.digits'                   => 'Bagian NIK hanya boleh diisi dengan angka dan berjumlah harus 16',
            'nama_bayi.required'           => 'Bagian nama bayi tidak boleh kosong',
            'nama_bayi.unique'             => 'Bagian nama bayi sudah terisi dengan nilai yang sama di dalam database, silahkan isi dengan nilai yang berbeda',
            'nama_ibu.required'            => 'Bagian nama ibu tidak boleh kosong',
            'nama_ayah.required'           => 'Bagian nama ayah tidak boleh kosong',
            'tgl_lahir_bayi.required'      => 'Bagian tanggal lahir tidak boleh kosong',
            'jenis_kelamin_bayi.required'  => 'Bagian jenis kelamin tidak boleh kosong',
            'berat_lahir_bayi.required'    => 'Bagian berat lahir tidak boleh kosong',
            'panjang_lahir_bayi.required'  => 'Bagian panjang lahir tidak boleh kosong'
        ];
    }
}