<?php

namespace App\Http\Requests\Registrasi\IbuHamil;

use App\Http\Requests\Request;

class CreateRequestIbuHamil extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'dasawisma_id'  => 'required',
            'tgl_entry'     => 'required',
            'nik'           => 'required|digits:16',
            'nama'          => 'required',
            'nama_suami'    => 'required',
            'umur_kehamilan'=> 'required',
            'tgl_lahir'     => 'required',
            'tgl_hamil'     => 'required'
        ];
    }

    public function messages()
    {
        return [
            'dasawisma_id.required'  => 'Bagian daerah dasawisma tidak boleh kosong',
            'tgl_entry.required'     => 'Bagian tanggal entry tidak boleh kosong',
            'nik.required'           => 'Bagian NIK tidak boleh kosong',
            'nik.digits'             => 'Bagian NIK hanya boleh diisi dengan angka dan berjumlah harus 16',
            'nama.required'          => 'Bagian nama tidak boleh kosong',
            'nama_suami.required'    => 'Bagian nama suami tidak boleh kosong',
            'umur_kehamilan.required'=> 'Bagian umur kehamilan tidak boleh kosong',
            'tgl_lahir.required'     => 'Bagian tanggal lahir tidak boleh kosong',
            'tgl_hamil.required'     => 'Bagian tanggal lahir tidak boleh kosong'
        ];
    }
}