<?php

namespace App\Http\Requests\MasterData\Banjar;

use App\Http\Requests\Request;

class CreateRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama_banjar' => 'required|unique:m_banjar,nama_banjar'
        ];
    }

    public function messages()
    {
        return [
            'nama_banjar.required'  => 'Bagian nama banjar tidak boleh kosong',
            'nama_banjar.unique'    => 'Nama Banjar sudah terisi dengan nilai yang sama. Silahkan masukan nama yang berbeda'
        ];
    }
}