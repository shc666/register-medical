<?php

namespace App\Http\Requests\MasterData\Banjar;

use App\Http\Requests\Request;

class UpdateRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama_banjar' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'nama_banjar.required'  => 'Bagian nama banjar tidak boleh kosong'
        ];
    }
}