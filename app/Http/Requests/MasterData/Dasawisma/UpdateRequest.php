<?php

namespace App\Http\Requests\MasterData\Dasawisma;

use App\Http\Requests\Request;

class UpdateRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'banjar_id'      => 'required',
            'nama_dasawisma' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'banjar_id.required'       => 'Bagian nama banjar tidak boleh kosong',
            'nama_dasawisma.required'  => 'Bagian nama dasawisma tidak boleh kosong',
        ];
    }
}