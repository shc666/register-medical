<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TBayiPoliData extends Model
{
    protected $table = 't_bayi_balita_polidata';

    protected $fillable = [
        'id',
        'bayi_id',
        'jadwal',
        'berat',
        'panjang',
        'asi',
        'vitamin_a1',
        'vitamin_a2',
        'oralit',
        'imun_hbo',
        'imun_bcg',
        'imun_dpthb1',
        'imun_dpthb2',
        'imun_dpthb3',
        'imun_polio1',
        'imun_polio2',
        'imun_polio3',
        'imun_polio4',
        'imun_campak',
        'is_set'
    ];
}
