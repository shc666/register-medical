<?php

namespace App\Listeners\Login;

use Carbon\Carbon;
use App\Events\User\LoggedIn;
use App\User;
use Auth;

class UpdateLastLoginTimestamp
{
    /**
     * Handle the event.
     *
     * @param LoggedIn $event
     * @return void
     */
    public function handle(LoggedIn $event)
    {
        $event = User::find(Auth::user()->id);
        $event->update(['last_login' => Carbon::now()]);
    }
}
