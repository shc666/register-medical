<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TIbuPoliData extends Model
{
    protected $table = 't_ibu_hamil_polidata';

    protected $fillable = [
        'ibu_id',
        'jadwal',
        'berat',
        'lila',
        'pmt',
        'tambah_darah_1',
        'tambah_darah_2',
        'tambah_darah_3',
        'imunisasi_tt_1',
        'imunisasi_tt_2',
        'imunisasi_tt_3',
        'imunisasi_tt_4',
        'imunisasi_tt_5',
        'vitamin_a',
        'catatan',
        'is_set'
    ];
}