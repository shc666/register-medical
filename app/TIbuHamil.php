<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TIbuHamil extends Model
{
    protected $table = 't_ibu_hamil';

    protected $fillable = [
        'dasawisma_id',
        'tgl_entry',
        'nik',
        'nama',
        'nama_suami',
        'tgl_lahir',
        'tgl_hamil',
        'umur',
        'umur_kehamilan',
        'hamil_count',
        'is_risk'
    ];
}