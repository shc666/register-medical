<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MBanjar extends Model
{
    protected $table = 'm_banjar';

    protected $fillable = ['nama_banjar'];
}