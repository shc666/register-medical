<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MDasawisma extends Model
{
    protected $table = 'm_dasawisma';

    protected $fillable = [
        'banjar_id',
        'nama_dasawisma'
    ];
}