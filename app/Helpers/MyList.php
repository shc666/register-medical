<?php

namespace App\Helpers;

use App\Helpers\List_util;

class MyList
{
    public static function list_filter( $list, $args = array(), $operator = 'AND' )
    {
        if ( ! is_array( $list ) ) {
            return array();
        }

        $util = new List_util( $list );

        return $util->filter( $args, $operator );
    }   
}