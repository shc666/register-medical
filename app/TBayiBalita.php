<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TBayiBalita extends Model
{
    protected $table = 't_bayi_balita';

    protected $fillable = [
        'dasawisma_id',
        'tgl_entry',
        'nik',
        'nama_bayi',
        'nama_ibu',
        'nama_ayah',
        'tgl_lahir_bayi',
        'jenis_kelamin_bayi',
        'berat_lahir_bayi',
        'panjang_lahir_bayi'
    ];
}