<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TWusPusPoliData extends Model
{
    protected $table = 't_wus_pus_polidata';

    protected $fillable = [
        'people_id',
        'anak_hidup',
        'anak_meninggal',
        'lila',
        'imunisasi_tt_1',
        'imunisasi_tt_2',
        'imunisasi_tt_3',
        'imunisasi_tt_4',
        'imunisasi_tt_5',
        'jenis_kontrasepsi_pakai',
        'tanggal_penggantian',
        'jenis_kontrasepsi_penggantian',
        'is_set'
    ];
}