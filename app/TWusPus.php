<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TWusPus extends Model
{
    protected $table = 't_wus_pus';

    protected $fillable = [
        'dasawisma_id',
        'tgl_entry',
        'nik',
        'nama',
        'nama_suami',
        'tgl_lahir',
        'umur',
        'tahapan_ks'
    ];
}