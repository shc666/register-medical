window.Vue = require('vue');

/** 
 * Main Structure Layouts
*/
// 

/**
 * Vue Router Callback And Global Plugins
 */
import Vue from 'vue'
import Router from './routes.js'
import VuePageTransition from 'vue-page-transition'
import VueProgressBar from 'vue-progressbar'

Vue.use(VueProgressBar, {
  color: 'rgb(143, 255, 199)',
  failedColor: 'red',
  height: '2px',
  thickness: '3px',
  transition: {
    speed: '0.5s',
    opacity: '0.6s',
    termination: 300
  },
})

Vue.use(VuePageTransition)

/**
 * Vue Render Init
 */
new Vue({
  el: '#app',
  router: Router,
  // components: {
  //   
  // },
});
