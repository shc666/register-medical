/**
 * Configure Vue Router
 */
import Vue from 'vue'
import VueRouter from 'vue-router'

/**
 * Pages View
 */
import RegistrasiIndex from './components/Registrasi/index.vue'
import RegistrasiCreate from './components/Registrasi/create.vue'
import PoliIndex from './components/Poli/index.vue'
import PoliCreate from './components/Poli/create.vue'

/**
 * Define routes
 */
Vue.use(VueRouter)
const router = new VueRouter({
    mode: 'history',
    linkExactActiveClass: 'active',
    linkActiveClass: 'active',
    routes: [
        {
            path: '/registrasi/:tipe',
            name: 'registrasi-index',
            props: true,
            component: RegistrasiIndex,
            meta: {
                title: 'Menu Registrasi | Register Medical',
                metaTags: [
                    {
                        name: 'description',
                        content: 'Menu Registrasi | Register Medical'
                    },
                    {
                        property: 'og:description',
                        content: 'Menu Registrasi | Register Medical'
                    }
                ]
            }
        },
        {
            path: '/registrasi/buat-data-baru/:tipe',
            name: 'registrasi-create',
            props: true,
            component: RegistrasiCreate,
            meta: {
                title: 'Form Buat Registrasi Baru | Register Medical',
                metaTags: [
                    {
                        name: 'description',
                        content: 'Halaman Form Buat Registrasi Baru | Register Medical'
                    },
                    {
                        property: 'og:description',
                        content: 'Halaman Form Buat Registrasi Baru | Register Medical'
                    }
                ]
            } 
        },
        {
            path: '/poli/:tipe/:reg_id',
            name: 'poli-index',
            props: true,
            component: PoliIndex,
            meta: {
                title: 'Daftar Poli Data | Register Medical',
                metaTags: [
                    {
                        name: 'description',
                        content: 'Halaman Index Poli Data | Register Medical'
                    },
                    {
                        property: 'og:description',
                        content: 'Halaman Index Poli Data | Register Medical'
                    }
                ]
            } 
        },
        {
            path: '/poli/:tipe/:reg_id/:id/ubah-data',
            name: 'poli-edit',
            props: true,
            component: PoliCreate,
            meta: {
                title: 'Form Poli Data | Register Medical',
                metaTags: [
                    {
                        name: 'description',
                        content: 'Halaman Form Poli Data | Register Medical'
                    },
                    {
                        property: 'og:description',
                        content: 'Halaman Form Poli Data | Register Medical'
                    }
                ]
            } 
        }
    ],
    
    /**
     *  Scroll Behavior On Every Page 
     */
    scrollBehavior (to, from, savedPosition) {
        if (savedPosition) {
          return savedPosition
        } else {
          return { x: 0, y: 0 }
        }
    }
});
 
/**
 * Metadata callback start. (This callback runs before every route change, including on page load).
 */
router.beforeEach((to, from, next) => {
    // This goes through the matched routes from last to first, finding the closest route with a title.
    // eg. if we have /some/deep/nested/route and /some, /deep, and /nested have titles, nested's will be chosen.
    const nearestWithTitle = to.matched.slice().reverse().find(r => r.meta && r.meta.title);
  
    // Find the nearest route element with meta tags.
    const nearestWithMeta = to.matched.slice().reverse().find(r => r.meta && r.meta.metaTags);
    const previousNearestWithMeta = from.matched.slice().reverse().find(r => r.meta && r.meta.metaTags);
  
    // If a route with a title was found, set the document (page) title to that value.
    if(nearestWithTitle) document.title = nearestWithTitle.meta.title;
  
    // Remove any stale meta tags from the document using the key attribute we set below.
    Array.from(document.querySelectorAll('[data-vue-router-controlled]')).map(el => el.parentNode.removeChild(el));
  
    // Skip rendering meta tags if there are none.
    if(!nearestWithMeta) return next();
  
    // Turn the meta tag definitions into actual elements in the head.
    nearestWithMeta.meta.metaTags.map(tagDef => {
      const tag = document.createElement('meta');
  
      Object.keys(tagDef).forEach(key => {
        tag.setAttribute(key, tagDef[key]);
      });
  
      // We use this to track which meta tags we create, so we don't interfere with other ones.
      tag.setAttribute('data-vue-router-controlled', '');
  
      return tag;
    })
    // Add the meta tags to the document head.
    .forEach(tag => document.head.appendChild(tag));
  
    next();
  });

export default router