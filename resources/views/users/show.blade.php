@extends('layouts.app')

@section('page-title')
    Lihat Pengguna
@endsection

@section('content')
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
        <!-- begin:: Content Head -->
        <div class="kt-subheader  kt-grid__item" id="kt_subheader">
            <div class="kt-container  kt-container--fluid ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">Lihat Pengguna</h3>
                    <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                    <span class="kt-subheader__desc">Lihat Pengguna</span>
                </div>
            </div>
        </div>
        <!-- end:: Content Head -->
        <!-- begin:: Content -->
        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
            <div class="row">
                <div class="col-lg-12">
                    <!--begin:: Portlet-->
                    <div class="kt-portlet ">
                        <div class="kt-portlet__body">
                            <div class="kt-widget kt-widget--user-profile-3">
                                <div class="kt-widget__top">
                                    <div class="kt-widget__content">
                                        <div class="kt-widget__head">
                                            <div class="kt-widget__action pull-left">
                                                <a class="btn btn-primary" href="{{ route('users.index') }}"> 
                                                    <span class="kt-portlet__head-icon">
                                                        <i class="flaticon2-back"></i>
                                                    </span> Kembali
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="kt-widget__bottom">
                                    <div class="kt-widget__item">
                                        <div class="kt-widget__icon">
                                            <i class="flaticon-user"></i>
                                        </div>
                                        <div class="kt-widget__details">
                                            <span class="kt-widget__title">Nama</span>
                                            <span class="kt-widget__value">{{ $user->name }}</span>
                                        </div>
                                    </div>
                                    <div class="kt-widget__item">
                                        <div class="kt-widget__icon">
                                            <i class="flaticon-profile"></i>
                                        </div>
                                        <div class="kt-widget__details">
                                            <span class="kt-widget__title">Username</span>
                                            <span class="kt-widget__value">{{ $user->username }}</span>
                                        </div>
                                    </div>
                                    <div class="kt-widget__item">
                                        <div class="kt-widget__icon">
                                            <i class="flaticon-email"></i>
                                        </div>
                                        <div class="kt-widget__details">
                                            <span class="kt-widget__title">Email</span>
                                            <span class="kt-widget__value">{{ $user->email }}</span>
                                        </div>
                                    </div>
                                    <div class="kt-widget__item">
                                        <div class="kt-widget__icon">
                                            <i class="flaticon2-lock"></i>
                                        </div>
                                        <div class="kt-widget__details">
                                            <span class="kt-widget__title">Roles</span>
                                            @if(!empty($user->getRoleNames()))
                                                @foreach($user->getRoleNames() as $v)
                                                    <label class="badge badge-success">{{ $v }}</label>
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end:: Portlet-->
                </div>
            </div>
        </div>
        <!-- end:: Content -->
    </div>
@endsection