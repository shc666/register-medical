<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
	<!-- begin::Head -->
	<head>
		<!--begin::Base Path (base relative path for assets of this page) -->
		<base href="">
        <!--end::Base Path -->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8" />
		<meta name="description" content="Updates and statistics">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- CSRF Token -->
		<meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('page-title') | {{ config('app.name', 'Register Medical') }}</title>

		<!--begin::Fonts -->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700">
		<!--end::Fonts -->

		<!--begin::Page Vendors Styles(used by this page) -->
		<link href="{{ url('assets/plugins/custom/fullcalendar/fullcalendar.bundle.css') }}" rel="stylesheet" type="text/css" />
		<!--end::Page Vendors Styles -->
		<!--begin::Global Theme Styles(used by all pages) -->
		<link href="{{ url('assets/plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ url('assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
		<!--end::Global Theme Styles -->
		<!--begin::Layout Skins(used by all pages) -->
		
		<!-- Custom Stylesheet -->
		<link href="{{ asset('/css/custom.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ url('assets/bootstrap-fileinput/css/fileinput.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ url('assets/bootstrap-fileinput/themes/explorer-fas/theme.css') }}" rel="stylesheet" type="text/css" />
        <!--end::Layout Skins -->
        <!-- begin::Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="{{ url('assets/media/logos/favicon.ico') }}" />
		<!-- end::Icons -->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
	</head>
	<!-- end::Head -->

	<!-- begin::Body -->
	<body class="kt-page--loading-enabled kt-page--loading kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-topbar kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-page--loading">
        <!-- begin::Page loader -->
        <div class="kt-page-loader kt-page-loader--logo">
			{{-- <img alt="Logo" src="{{ url('assets/media/logos/logo-mini-md.png') }}"/>
			<div class="kt-loader--title">
				<h4 class="mt-3">DPMD PROVINSI BALI</h4>
			</div> --}}
            <div class="kt-spinner kt-spinner--danger"></div>
        </div>
		<!-- end::Page Loader -->
		<!-- begin:: Page -->
		<!-- begin:: Header Mobile -->
		<div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
			<div class="kt-header-mobile__logo">
				<a href="{{ route('dashboard') }}">
					{{-- <img alt="Logo" src="{{ url('assets/media/logos/logo-light.png') }}" /> --}}
				</a>
			</div>
			<div class="kt-header-mobile__toolbar">
				<button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler">
					<span></span>
				</button>
				<button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler">
					<i class="flaticon-more-1"></i>
				</button>
			</div>
		</div>
		<!-- end:: Header Mobile -->
		<div class="kt-grid kt-grid--hor kt-grid--root">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
				<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

					@include('partials.header')

					<div id="app">

						<!-- content -->
						@yield('content')
						<vue-progress-bar></vue-progress-bar>

					</div>

					@include('partials.footer')

				</div>
			</div>
		</div>
		<!-- end:: Page -->
		<!-- begin::Scrolltop -->
		<div id="kt_scrolltop" class="kt-scrolltop">
			<i class="fa fa-arrow-up"></i>
		</div>
		<!-- end::Scrolltop -->
		<!-- begin::Global Config(global config for global JS sciprts) -->
		<script>
			var KTAppOptions = {
				"colors": {
					"state": {
						"brand": "#5d78ff",
						"dark": "#282a3c",
						"light": "#ffffff",
						"primary": "#5867dd",
						"success": "#34bfa3",
						"info": "#36a3f7",
						"warning": "#ffb822",
						"danger": "#fd3995"
					},
					"base": {
						"label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
						"shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
					}
				}
			};
		</script>
		<!-- end::Global Config -->
		<!--begin::Global Theme Bundle(used by all pages) -->
		<script src="{{ url('assets/plugins/global/plugins.bundle.js') }}" type="text/javascript"></script>
		<script src="{{ url('assets/js/scripts.bundle.js') }}" type="text/javascript"></script>
		<!--end::Global Theme Bundle -->
		<!--begin::Page Vendors(used by this page) -->
		<script src="{{ url('assets/plugins/custom/fullcalendar/fullcalendar.bundle.js') }}" type="text/javascript"></script>
		<script src="{{ url('assets/plugins/custom/gmaps/gmaps.js') }}" type="text/javascript"></script>
		<!--end::Page Vendors -->
		<!--begin::Page Scripts(used by this page) -->
		<script src="{{ url('assets/plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
		<script src="{{ url('assets/js/pages/dashboard.js') }}" type="text/javascript"></script>
		<script src="{{ url('assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js') }}" type="text/javascript"></script>
		<script src="{{ url('assets/js/pages/crud/forms/widgets/bootstrap-select.js') }}" type="text/javascript"></script>
		<script src="{{ url('assets/js/pages/crud/forms/widgets/select2.js') }}" type="text/javascript"></script>
		<script src="{{ url('assets/bootstrap-fileinput/js/plugins/piexif.js') }}" type="text/javascript"></script>
		<script src="{{ url('assets/bootstrap-fileinput/js/plugins/sortable.js') }}" type="text/javascript"></script>
		<script src="{{ url('assets/bootstrap-fileinput/js/fileinput.js') }}" type="text/javascript"></script>
		<script src="{{ url('assets/bootstrap-fileinput/js/locales/id.js') }}" type="text/javascript"></script>
		<script src="{{ url('assets/bootstrap-fileinput/themes/fas/theme.js') }}" type="text/javascript"></script>
		<script src="{{ url('assets/bootstrap-fileinput/themes/explorer-fas/theme.js') }}" type="text/javascript"></script>
		<script src="{{ url('assets/js/pages/crud/forms/validation/form-controls.js') }}" type="text/javascript"></script>
		<script src="{{ url('assets/js/pages/crud/file-upload/ktavatar.js') }}" type="text/javascript"></script>
		<script src="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
		{!! Toastr::message() !!}
		<!--end::Page Scripts -->
		<script src="{{ url('assets/js/as/app.js') }}" type="text/javascript"></script>

		{{-- VUE JS --}}
		<script src="{{ asset ('/js/app.js')}}"></script>

        @yield('scripts')

	</body>
	<!-- end::Body -->
</html>