@extends('layouts.app')

@section('content')
    <vue-page-transition name="fade-in-left">
        <router-view></router-view>
    </vue-page-transition>
@endsection