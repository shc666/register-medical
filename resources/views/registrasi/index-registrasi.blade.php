@extends('layouts.app')

@section('content')
    <vue-page-transition name="fade-in-right">
        <router-view></router-view>
    </vue-page-transition>
@endsection