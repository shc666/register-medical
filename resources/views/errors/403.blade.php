@extends('layouts.errors')

@section('title', '403 - Akses Ditolak!')

@section('content')
    <!-- begin:: Page -->
    <div class="kt-grid kt-grid--ver kt-grid--root kt-page">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid  kt-error-v1" style="background-image: url({{ url('assets/media/error/bg1.jpg') }});">
            <div class="kt-error-v1__container">
                <h1 class="kt-error-v1__number">403</h1>
                <p class="kt-error-v1__desc">
                    Anda tidak mempunyai hak akses yang cukup untuk mengakses halaman ini! 
                    <button type="button" class="btn btn-outline-danger btn-elevate btn-pill" onclick="window.location.href='{{ route('dashboard') }}'">
                        <i class="flaticon-exclamation"></i> 
                        Kembali
                    </button>
                </p>
            </div>
        </div>
    </div>
    <!-- end:: Page -->
@endsection