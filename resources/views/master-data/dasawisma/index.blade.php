@extends('layouts.app')

@section('page-title')
    Master Data Dasawisma
@endsection

@section('content')
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
        <!-- begin:: Content Head -->
        <div class="kt-subheader  kt-grid__item" id="kt_subheader">
            <div class="kt-container  kt-container--fluid ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">Dasawisma</h3>
                    <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                    <span class="kt-subheader__desc">Master Data Dasawisma</span>
                </div>
            </div>
        </div>
        <!-- end:: Content Head -->
        <!-- begin:: Content -->
        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
            <div class="row">
                <div class="col-lg-12">

                    @include('partials.messages')
                    
                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <span class="kt-portlet__head-icon">
                                    <i class="flaticon2-layers"></i>
                                </span>
                                <h3 class="kt-portlet__head-title">
                                    Data Dasawisma
                                </h3>
                            </div>
                            <div class="kt-portlet__head-toolbar">
                                <div class="kt-portlet__head-actions">
                                    <a class="btn btn-success" href="{{ route('dasawisma.create') }}"> 
                                        <i class="flaticon2-plus"></i>
                                        Buat Data Baru
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="kt-portlet .kt-portlet--mobile">
                            <div class="kt-portlet__body">
                                <!--begin::Section-->
                                <div class="kt-section">
                                    <div class="kt-section__content">
                                        <div class="table-responsive">
                                            <!--begin: Datatable -->
                                            <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                                                <thead>
                                                    <tr>
                                                        <th>No.</th>                                
                                                        <th>Nama Banjar</th>
                                                        <th>Nama Dasawisma</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    
                                                </tbody>
                                            </table>
                                            <!--end: Datatable -->
                                        </div>
                                    </div>
                                </div>
                                <!--end::Section-->
                            </div>
                        </div>
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>
        </div>
        <!-- end:: Content -->
    </div>
@endsection

@section('scripts')
    <script src="{{ url('assets/js/ajax-master-dasawisma.js') }}" type="text/javascript"></script>
@endsection