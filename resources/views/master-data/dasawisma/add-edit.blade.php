@extends('layouts.app')

@section('page-title')
    {{ $edit ? trans('app.update_dasawisma') : trans('app.create_dasawisma') }}
@endsection

@section('content')
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
        <!-- begin:: Content Head -->
        <div class="kt-subheader  kt-grid__item" id="kt_subheader">
            <div class="kt-container  kt-container--fluid ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">Dasawisma</h3>
                    <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                    <span class="kt-subheader__desc">{{ $edit ? trans('app.update_dasawisma') : trans('app.create_dasawisma') }}</span>
                </div>
            </div>
        </div>
        <!-- end:: Content Head -->
        <!-- begin:: Content -->
        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
            <div class="row">
                <div class="col-lg-12">
                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label wrapper-back__button">
                                <div class="col-md-4 back-button mr-3">
                                    <a class="btn btn-primary" href="{{ route('dasawisma.index') }}">
                                        <i class="fa fa-angle-left"></i> 
                                        Kembali
                                    </a>
                                </div>
                            </div>
                        </div>

                        @include('partials.messages')
                        
                        <!--begin::Form-->
                        @if($edit)
                            {!! Form::open(['route' => ['dasawisma.update', $data->id], 'method' => 'PUT', 'class' => 'kt-form kt-form--label-right', 'id' => 'kt_form_1', 'enctype' => 'multipart/form-data']) !!}
                        @else
                            {!! Form::open(['route' => 'dasawisma.store', 'class' => 'kt-form kt-form--label-right', 'id' => 'kt_form_1', 'enctype' => 'multipart/form-data']) !!}
                        @endif
                            <div class="kt-portlet__body">
                                <div class="form-group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">Nama Banjar*</label>
                                    <div class="col-lg-6 col-sm-12">
                                        <select class="form-control kt-select2" name="banjar_id" id="kt_select2_4">
                                            <option value=""></option>
                                            @foreach($banjars as $banjar)
                                                @if(isset($data->banjar_id) && $data->banjar_id == $banjar->id)
                                                    <option value="{{ $banjar->id }}" selected >{{ $banjar->nama_banjar }}</option>
                                                @else
                                                    <option value="{{ $banjar->id }}">{{ $banjar->nama_banjar }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">Nama Dasawisma*</label>
                                    <div class="col-lg-6 col-sm-12">
                                        <input type="text" name="nama_dasawisma" class="form-control" placeholder="Masukkan Nama Dasawisma" value="{{ $edit ? $data->nama_dasawisma : old('nama_dasawisma') }}" id="nama_dasawisma">
                                    </div>
                                </div>
                            </div>
                            <div class="kt-portlet__foot">
                                <div class="kt-form__actions kt-form__actions--right">
                                    @if($edit)
                                        {{-- <button type="button" class="btn btn-danger mr-3" id="reset">
                                            <i class="flaticon2-circular-arrow"></i>
                                            Reset
                                        </button> --}}
                                        <button type="submit" class="btn btn-success">
                                            <i class="flaticon2-checkmark"></i>
                                            {{ $edit ? trans('app.update') : trans('app.save') }}
                                        </button>
                                    @else
                                        <button type="submit" class="btn btn-success">
                                            <i class="flaticon2-checkmark"></i>
                                            {{ $edit ? trans('app.update') : trans('app.save') }}
                                        </button>
                                    @endif
                                </div>
                            </div>
                        {!! Form::close() !!}
                        <!--end::Form-->
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>
        </div>
        <!-- end:: Content -->
    </div>
@endsection