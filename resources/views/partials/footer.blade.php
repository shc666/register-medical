<!-- begin:: Footer -->
<div class="kt-footer  kt-footer--basic  kt-grid__item" id="kt_footer">
    <div class="kt-footer__bottom">
        <div class="kt-container">
            <div class="kt-footer__wrapper">
                <div class="kt-footer__logo">
                    <a href="index.html">
                        <img alt="Logo" src="{{ url('assets/media/logos/logo-badung-2-sm.png') }}">
                    </a>
                    <div class="kt-footer__copyright">
                        © {{ date('Y') }} Desa Punggul
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end:: Footer -->