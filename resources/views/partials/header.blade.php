<!-- begin:: Header -->
<div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed " data-ktheader-minimize="on">
    <div class="kt-header__top">
        <div class="kt-container ">
            <!-- begin:: Brand -->
            <div class="kt-header__brand   kt-grid__item" id="kt_header_brand">
                <div class="kt-header__brand-logo">
                    <a href="{{ route('dashboard') }}">
                        <img alt="Logo" src="{{ url('assets/media/logos/logo-badung-2.png') }}" class="kt-header__brand-logo-default" />
                        <img alt="Logo" src="{{ url('assets/media/logos/logo-badung-2-sm.png') }}" class="kt-header__brand-logo-sticky" />
                    </a>
                </div>
            </div>
            <!-- end:: Brand -->
            <!-- begin:: Header Topbar -->
            <div class="kt-header__topbar">
                <!--begin: User bar -->
                <div class="kt-header__topbar-item kt-header__topbar-item--user">
                    <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,10px" aria-expanded="false">
                        <span class="kt-header__topbar-welcome">Hi,</span>
                        <span class="kt-header__topbar-username">{{ Auth::user()->name }}</span>
                        @isset(Auth::user()->avatar)
                            <img alt="Pic" src="{{ asset("upload/avatars/".Auth::user()->avatar) }}" />
                        @else
                            <img alt="Pic" src="{{ url('assets/media/users/default-circle.png') }}" />
                        @endisset
                    </div>
                    <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-xl">
                        <!--begin: Head -->
                        <div class="kt-user-card kt-user-card--skin-dark kt-notification-item-padding-x" style="background-image: url({{ url('assets/media/misc/bg-1.jpg') }})">
                            <div class="kt-user-card__avatar">
                                @isset(Auth::user()->avatar)
                                    <img class="kt-hidden" alt="Pic" src="{{ asset("upload/avatars/".Auth::user()->avatar) }}" />
                                @else
                                    <img class="kt-hidden" alt="Pic" src="{{ url('assets/media/users/default-circle.png') }}" />
                                @endisset
                                <!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
                                <span class="kt-badge kt-badge--lg kt-badge--rounded kt-badge--bold kt-font-success">
                                    @php
                                        $split = explode(' ', Auth::user()->name);
                                        $name1 = str_limit($split[0], $limit = 1, $end = '');
                                        $name2 = str_limit($split[1], $limit = 1, $end = '');
                                    @endphp
                                    {{ $name1 }}{{ $name2}}
                                </span>
                            </div>
                            <div class="kt-user-card__name">
                                {{ Auth::user()->name }}
                            </div>
                        </div>
                        <!--end: Head -->
                        <!--begin: Navigation -->
                        <div class="kt-notification">
                            <a href="{{ route('profiles.index') }}" class="kt-notification__item">
                                <div class="kt-notification__item-icon">
                                    <i class="flaticon2-calendar-3 kt-font-success"></i>
                                </div>
                                <div class="kt-notification__item-details">
                                    <div class="kt-notification__item-title kt-font-bold">
                                        Profil Saya
                                    </div>
                                    <div class="kt-notification__item-time">
                                        Pengaturan Akun dan Password
                                    </div>
                                </div>
                            </a>
                            <div class="kt-notification__custom kt-space-between">
                                <a href="{{ route('auth.logout') }}" class="btn btn-label btn-label-brand btn-sm btn-bold">@lang('app.logout')</a>
                            </div>
                        </div>
                        <!--end: Navigation -->
                    </div>
                </div>
                <!--end: User bar -->
            </div>
            <!-- end:: Header Topbar -->
        </div>
    </div>
    <div class="kt-header__bottom">
        <div class="kt-container ">

            @include('partials.header-sidebar')
        
        </div>
    </div>
</div>
<!-- end:: Header -->