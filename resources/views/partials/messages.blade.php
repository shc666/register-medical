@if(isset ($errors) && count($errors) > 0)
    <div class="alert alert-danger alert-notification" role="alert">
        <div class="alert-icon">
            <i class="flaticon2-cross"></i>
        </div>
        <ul class="list-unstyled mb-0">
            @foreach($errors->all() as $error)
                <li class="alert-text">{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if(Session::get('success', false))
<?php $data = Session::get('success'); ?>
    @if (is_array($data))
        @foreach ($data as $msg)
            <div class="alert alert-success alert-notification" role="alert">
                <div class="alert-icon">
                    <i class="flaticon2-checkmark"></i>
                </div>
                <div class="alert-text">{{ $msg }}</div>
            </div>
        @endforeach
    @else
        <div class="alert alert-success alert-notification" role="alert">
            <div class="alert-icon">
                <i class="flaticon2-checkmark"></i>
            </div>
            <div class="alert-text">{{ $data }}</div>
        </div>
    @endif
@endif