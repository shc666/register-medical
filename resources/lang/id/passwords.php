<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reminder Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password'  => 'Password harus berupa enam karakter dan sesuai dengan konfirmasi password.',
    'reset'     => 'Password anda telah direset!',
    'sent'      => 'Kami telah mengirimkan link reset password!',
    'token'     => 'Token reset password ini tidak valid.',
    'user'      => "Kami tidak dapat menemukan alamat email yang dimasukkan.",

];
