<?php

/**
 * Authentication
 */
Route::get('login', 'Auth\AuthController@getLogin')->name('login');
Route::post('login', 'Auth\AuthController@postLogin')->name('post.login');

Route::get('logout', [
    'as' => 'auth.logout',
    'uses' => 'Auth\AuthController@getLogout'
]);

Route::group(['middleware' => 'auth'], function () {

    /**
     * Dashboard
     */
    Route::get('/', [
        'as' => 'dashboard',
        'uses' => 'DashboardController@index'
    ]);

    /**
     * Profiles Details
     */
    Route::get('/profile', [
        'as' => 'profiles.index',
        'uses' => 'ProfileController@index'
    ]);
    Route::post('/profile/profile', [
        'as' => 'profiles.profile',
        'uses' => 'ProfileController@profileUpdate'
    ]);
    Route::post('/profile/account', [
        'as' => 'profiles.account',
        'uses' => 'ProfileController@accountUpdate'
    ]);
    Route::post('/profile/password', [
        'as' => 'profiles.password',
        'uses' => 'ProfileController@passwordUpdate'
    ]);

    /*
    * Users, Role, and Permissions
    */
    Route::resource('users','UserController');
    Route::resource('roles','RoleController');
    Route::resource('permissions','PermissionsController');

    /* Master Data */
    Route::resource('banjar','BanjarController', [
        'except' => 'show'
    ]);
    Route::get('banjar/dtb-banjar', [
        'as' => 'get.dtb-banjar',
        'uses' => 'BanjarController@getData'
    ]);
    Route::resource('dasawisma','DasawismaController', [
        'except' => 'show'
    ]);
    Route::get('dasawisma/dtb-dasawisma', [
        'as' => 'get.dtb-dasawisma',
        'uses' => 'DasawismaController@getData'
    ]);

    /* Registrasi */
    /* Define views for Laravel and reserved routes for Vue*/
    Route::get('registrasi/{tipe}',[
        'as' => 'registrasi.tipe',
        'uses' => 'RegistrasiController@indexRegistrasi'
    ]);
    Route::get('registrasi/buat-data-baru/{tipe}',[
        'as' => 'create.registrasi.tipe',
        'uses' => 'RegistrasiController@create'
    ]);
    Route::get('poli/{tipe}/{reg_id}', [
        'as' => 'poli.tipe',
        'uses' => 'RegistrasiController@indexPoli'
    ]);
    Route::get('poli/{tipe}/{reg_id}/{id}/ubah-data', [
        'as' => 'registrasi.edit',
        'uses' => 'RegistrasiController@edit'
    ]);

    /* For Laravel and Vue Controller */
    Route::get('data-registrasi/registrasi/{tipe}', [
        'as' => 'index.registrasi',
        'uses' => 'RegistrasiController@getData'
    ]);
    Route::get('data-registrasi/registrasi/dtb-registrasi/{tipe}', [
        'as' => 'get.dtb-registrasi',
        'uses' => 'RegistrasiController@getDataRegistrasi'
    ]);
    Route::get('data-poli/poli/dtb-poli/{tipe}/{reg_id}', [
        'as' => 'get.dtb-poli',
        'uses' => 'RegistrasiController@getDataPoli'
    ]);
    Route::get('data-poli/poli/{tipe}/{reg_id}/{id}', [
        'as' => 'get.data-poli',
        'uses' => 'RegistrasiController@getPrevData'
    ]);
    Route::post('registrasi/store-balita', [
        'as' => 'registrasi.balita.post',
        'uses' => 'RegistrasiController@storeBalita'
    ]);
    Route::post('registrasi/store-ibuhamil', [
        'as' => 'registrasi.ibuhamil.post',
        'uses' => 'RegistrasiController@storeIbuHamil'
    ]);
    Route::post('registrasi/store-wuspus', [
        'as' => 'registrasi.wuspus.post',
        'uses' => 'RegistrasiController@storeWusPus'
    ]);
    Route::post('poli/store-balita', [
        'as' => 'poli.balita.post',
        'uses' => 'RegistrasiController@storePoliBalita'
    ]);
    Route::post('poli/store-ibu-hamil', [
        'as' => 'poli.ibuhamil.post',
        'uses' => 'RegistrasiController@storePoliIbuHamil'
    ]);
    Route::post('poli/store-wus-pus', [
        'as' => 'poli.wuspus.post',
        'uses' => 'RegistrasiController@storePoliWusPus'
    ]);
    Route::delete('registrasi/delete-balita/{reg_id}', [
        'as' => 'registrasi.balita.delete',
        'uses' => 'RegistrasiController@destroyRegistrasiBalita'
    ]);
    Route::delete('registrasi/delete-ibu-hamil/{reg_id}', [
        'as' => 'registrasi.ibuhamil.delete',
        'uses' => 'RegistrasiController@destroyRegistrasiIbuHamil'
    ]);
    Route::delete('registrasi/delete-wus-pus/{reg_id}', [
        'as' => 'registrasi.wuspus.delete',
        'uses' => 'RegistrasiController@destroyRegistrasiWusPus'
    ]);

    /* Laporan */
    Route::get('laporan', [
        'as' => 'laporan.index',
        'uses' => 'LaporanController@index'
    ]);
});