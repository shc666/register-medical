var as = {};

as.hideNotifications = function () {
    $(".alert-notification").slideUp(800, function () {
        $(this).remove();
    })
};

as.init = function () {

    $.ajaxSetup({
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
    });

    $(".alert-notification .close").click(as.hideNotifications);

    setTimeout(as.hideNotifications, 3500);

};

$(document).ready(as.init);