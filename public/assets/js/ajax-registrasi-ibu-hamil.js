'use strict';
var KTDatatablesDataSourceAjaxServer = function() {

	var initTable1 = function() {
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		var table = $('#kt_table_1');

		// begin first table
		table.DataTable({
			destroy: true,
			responsive: true,
			searchDelay: 500,
			processing: true,
            serverSide: true,
            scrollX: true,
			ajax: {
				url: 'http://localhost:8000/registrasi/dtb-registrasi/2',
				type: 'GET'
			},
			columns: [
				{data: 'DT_RowIndex', type:"text", filter: false, width: '5%'},
				{data: 'tgl_lahir'},
				{data: 'tgl_hamil'},
				{data: 'hamil_count'},
				{data: 'nik'},
				{data: 'nama'},
				{data: 'nama_suami'},
				{data: 'tgl_entry'},
				{data: 'action', width: '15%'}
			],
			language: {
                "sProcessing": "Sedang memproses...",
                "sLengthMenu": "Tampilkan _MENU_ entri",
                "sZeroRecords": "Tidak ditemukan data yang sesuai",
                "sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                "sInfoEmpty": "Menampilkan 0 sampai 0 dari 0 entri",
                "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                "sInfoPostFix": "",
                "sSearch": "Cari:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Pertama",
                    "sPrevious": "Sebelumnya",
                    "sNext": "Selanjutnya",
                    "sLast": "Terakhir"
                }
            },
			columnDefs: [
				{
					targets: -2,
					render: function(data, type, full, meta) {
						var status = {
							1: {'title': 'Belum Diverifikasi', 'class': 'kt-badge--info'},
							2: {'title': 'Diverifikasi', 'class': ' kt-badge--success'},
							3: {'title': 'Ditolak', 'class': ' kt-badge--danger'},
							4: {'title': 'Belum Layak', 'class': ' kt-badge--warning'}
						};
						if (typeof status[data] === 'undefined') {
							return data;
						}
						return '<span class="kt-badge ' + status[data].class + ' kt-badge--inline kt-badge--pill">' + status[data].title + '</span>';
					},
				},
			]
		});
	};

	$('#submit').click(function (){
		$('#kt_table_1').DataTable().draw(true);
	});

	return {

		//main function to initiate the module
		init: function() {
			initTable1();
		},
	};

}();

jQuery(document).ready(function() {
	KTDatatablesDataSourceAjaxServer.init();
});

var Delete = {
	"data" : function(rowid){
		swal.fire({
			title: 'Apakah anda yakin akan menghapus ini?',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Ya',
			cancelButtonText: 'Batal!',
			reverseButtons: true
		}).then(function(result){
			if(result.value) {
				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});
				$.ajax({
					url: "http://localhost:8000/banjar/" + rowid,
					type: 'DELETE',
					dataType: "json",
					success: function(){
						swal.fire(
							'Sukses',
							'Data telah berhasil terhapus!.',
							'success'
						),
							setTimeout(function() {
							window.location.reload();
						}, 2000);
					}
				});
			} else if(result.dismiss === 'cancel') {
				swal.fire(
					'',
					'Batal menghapus data!',
					'error'
				)
			}
		});
	},
};