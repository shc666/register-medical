// Class definition

var KTFormControls = function () {
    // Private functions
    
    var demo1 = function () {
        $( "#kt_form_1" ).validate({
            // define validation rules
            rules: {
                email: {
                    required: true,
                    email: true,
                    minlength: 10 
                },
                name: {
                    required: true 
                },
                username: {
                    required: true
                },
                password: {
                    required: true,
                    minlength: 8
                },
                password: {
                    required: true,
                    minlength: 8
                },
                confirm: {
                    required: true,
                    minlength: 8,
                    equalTo: "#password"
                },
                roles: {
                    required: true
                },
                permission: {
                    required: true
                },
                display_name: {
                    required: true
                },
                description: {
                    required: true
                },
                no_urut: {
                    required: true,
                    digits:true
                },
                nama: {
                    required: true
                },
                jenis_kelamin: {
                    required: true
                },
                tempat_lahir: {
                    required: true
                },
                tgl_lahir: {
                    required: true
                },
                tgl_lahir_bayi: {
                    required: true
                },
                tgl_entry: {
                    required: true
                },
                berat_lahir_bayi: {
                    required: true,
                    digits: true
                },
                panjang_lahir_bayi: {
                    required: true,
                    digits: true
                },
                nip: {
                    required: true,
                    digits: true
                },
                nama_banjar: {
                    required: true
                },
                nama_dasawisma: {
                    required: true
                },
                checkbox: {
                    required: true
                },
                nama_bayi: {
                    required: true
                },
                nama_ayah: {
                    required: true
                },
                nama_ibu: {
                    required: true
                },
                tgl_kesepakatan: {
                    required: true
                },
                banjar_id: {
                    required: true
                },
                tag: {
                    required: true
                },
                nik: {
                    required: true
                },
                jenis_kelamin_bayi: {
                    required: true
                },
                files: {
                    required: true
                },
                dasawisma: {
                    required: true
                },
                options: {
                    required: true,
                    minlength: 2,
                    maxlength: 4
                },
                memo: {
                    required: true,
                    minlength: 10,
                    maxlength: 100
                },
                phone: {
                    required: true,
                    phoneUS: true 
                },
                checkbox: {
                    required: true
                },
                checkboxes: {
                    required: true,
                    minlength: 1,
                    maxlength: 2
                },
                radio: {
                    required: true
                }
            },
            messages: {
                name: "Bagian nama tidak boleh kosong.",
                email: "Bagian email tidak boleh kosong.",
                username: "Bagian username tidak boleh kosong.",
                password: {
                    required: "Bagian password tidak boleh kosong.",
                    minlength: "Bagian password minimal 8 karakter"
                },
                confirm: {
                    required: "Bagian konfirmasi password tidak boleh kosong.",
                    minlength: "Bagian konfirmasi password minimal 8 karakter",
                    equalTo: "Bagian konfirmasi password harus sama dengan password"
                },
                roles: "Bagian roles tidak boleh kosong.",
                permission: "Bagian permission tidak boleh kosong.",
                display_name: "Bagian tampilan permission tidak boleh kosong.",
                description: "Bagian deskripsi tidak boleh kosong.",
                no_urut: "Bagian nomor urut tidak boleh kosong dan hanya format angka.",
                nama: "Bagian nama tidak boleh kosong.",
                jenis_kelamin: "Bagian jenis kelamin tidak boleh kosong.",
                tempat_lahir: "Bagian tempat lahir tidak boleh kosong.",
                tgl_lahir: "Bagian tanggal lahir tidak boleh kosong.",
                tgl_lahir_bayi: "Bagian tanggal lahir bayi tidak boleh kosong.",
                tgl_entry: "Bagian tanggal memasukan tidak boleh kosong.",
                nip_aparat_pemerintah: "Bagian nip aparat tidak boleh kosong dan hanya format angka.",
                nip: "Bagian nip tidak boleh kosong dan hanya format angka.",
                nama_banjar: "Bagian nama banjar tidak boleh kosong",
                nama_dasawisma: "Bagian nama dasawisma tidak boleh kosong",
                nama_bayi: "Bagian nama bayi tidak boleh kosong",
                nama_ayah: "Bagian nama ayah tidak boleh kosong",
                nama_ibu: "Bagian nama ibu tidak boleh kosong",
                tgl_kesepakatan: "Bagian tanggal kesepakatan tidak boleh kosong",
                tag: "Bagian tag tidak boleh kosong",
                banjar_id: "Bagian banjar tidak boleh kosong",
                files: "Bagian files tidak boleh kosong",
                dasawisma: "Bagian dasawisma tidak boleh kosong",
                berat_lahir_bayi: "Bagian berat lahir bayi tidak boleh kosong",
                jenis_kelamin_bayi: "Bagian jenis kelamin bayi tidak boleh kosong",
                panjang_lahir_bayi: "Bagian panjang lahir bayi tidak boleh kosong"

            },
            
            //display error alert on form submit  
            invalidHandler: function(event, validator) {     
                var alert = $('#kt_form_1_msg');
                alert.removeClass('kt--hide').show();
                KTUtil.scrollTop();
                swal.fire({
                    "title": "", 
                    "text": "Masih terdapat bagian yang kosong. Silahkan periksa kembali.", 
                    "type": "error",
                    "showConfirmButton": false,
                    "timer": 3000
                });
            },

            // submitHandler: function (form) {
            //     //form[0].submit(); // submit the form
            // }
        });       
    }

    var demo2 = function () {
        $( "#kt_form_2" ).validate({
            // define validation rules
            rules: {
                //= Client Information(step 3)
                // Billing Information
                billing_card_name: {
                    required: true
                },
                billing_card_number: {
                    required: true,
                    creditcard: true
                },
                billing_card_exp_month: {
                    required: true
                },
                billing_card_exp_year: {
                    required: true
                },
                billing_card_cvv: {
                    required: true,
                    minlength: 2,
                    maxlength: 3
                },

                // Billing Address
                billing_address_1: {
                    required: true
                },
                billing_address_2: {
                    
                },
                billing_city: {
                    required: true
                },
                billing_state: {
                    required: true
                },
                billing_zip: {
                    required: true,
                    number: true
                },

                billing_delivery: {
                    required: true
                }
            },
            
            //display error alert on form submit  
            invalidHandler: function(event, validator) {
                swal.fire({
                    "title": "", 
                    "text": "There are some errors in your submission. Please correct them.", 
                    "type": "error",
                    "confirmButtonClass": "btn btn-secondary",
                    "onClose": function(e) {
                        console.log('on close event fired!');
                    }
                });

                event.preventDefault();
            },

            submitHandler: function (form) {
                //form[0].submit(); // submit the form
                swal.fire({
                    "title": "", 
                    "text": "Form validation passed. All good!", 
                    "type": "success",
                    "confirmButtonClass": "btn btn-secondary"
                });

                return false;
            }
        });       
    }

    return {
        // public functions
        init: function() {
            demo1(); 
            demo2();
        }
    };
}();

jQuery(document).ready(function() {    
    KTFormControls.init();
});